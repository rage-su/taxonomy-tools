/*	========================================================

	TAXONOMY-LANG.JS
	
	This file contains localization strings for the UI of
	the RAGE Taxonomy Tools.
	
	License: to be defined
	
	Project RAGE (No 644187)
	Realising and Applied Gaming Eco-system
	Research and Innovation Action

	2015 P. Boytchev
	
	========================================================
*/

TaxonomyLang =
{
	// title of the RAGE Taxonomy Viewer
	VIEWER: {	en: 'Taxonomy Viewer',
				bg: 'Преглед на таксономия',
				de: 'Betrachter der Taxonomie',
				es: 'Visor de la taxonomía',
				fr: 'Observateur de la taxonomie',
				it: 'Visualizzatore di tassonomia',
				nl: 'Kijker van de taxonomie',
				pt: 'Visualizador de taxonomia',
				ro: 'Vizualizator de taxonomie'
			},
			
	// title of the RAGE Taxonomy Selector
	SELECTOR: {	en: 'Taxonomy Selector',
				bg: 'Избор от таксономия',
				de: 'Selector der Taxonomie',
				es: 'Selector de la taxonomía',
				fr: 'Sélecteur de la taxonomie',
				it: 'Selettore di tassonomia',
				nl: 'Selector van de taxonomie',
				pt: 'Seletor de taxonomia',
				ro:'Selector de taxonomie'
			},
			
	// title of the RAGE Taxonomy Editor
	EDITOR: {	en: 'Taxonomy Editor',
				bg: 'Редактор на таксономия',
				de: 'Editor der Taxonomie',
				es: 'Editor de la taxonomía',
				fr: 'Éditeur de taxonomie',
				it: 'Editor di tassonomia',
				nl: 'Redacteur van de taxonomie',
				pt: 'Editor de taxonomia',
				ro: 'Editor de taxonomie'
			},
	
	// caption of the [Layout] button
	LAYOUT: {	en: 'Layout',
				bg: 'Изглед',
				de: 'Layout',
				es: 'Diseño',
				fr: 'Disposition',
				it: 'Impaginazione',
				nl: 'Lay-out',
				pt: 'Traçado',
				ro: 'Schemă'
			},
			
	// caption of the [Language] button
	LANGUAGE: {	en: 'Language',
				bg: 'Език',
				de: 'Sprache',
				es: 'Idioma',
				fr: 'Langue',
				it: 'Lingua',
				nl: 'Taal',
				pt: 'Idioma',
				ro: 'Limbă'
			},

	CONNECT: {	en: 'Connecting...',
				bg: 'Свързване...',
				de: 'Anschließen...',
				es: 'Conexión...',
				fr: 'De liaison...',
				it: 'Collegamento...',
				nl: 'Aansluiten...',
				pt: 'Ligar...',
				ro: 'Conectarea...'
			},
			
	FAILED: {	en: 'Connection failed',
				bg: 'Свързването е неуспешно',
				de: 'Verbindung fehlgeschlagen',
				es: 'Conexión fallida',
				fr: 'La connexion a échoué',
				it: 'Connessione fallita',
				nl: 'Verbinding mislukt',
				pt: 'A ligação falhou',
				ro: 'Conexiune esuata'
			},
			
	TIMEOUT: {	en: 'Connection timeout',
				bg: 'Свързването е прекратено',
				de: 'Verbindungs-Timeout',
				es: 'El tiempo de conexión expiro',
				fr: 'Délai de connection dépassé',
				it: 'Timeout connessione',
				nl: 'Verbinding time-out',
				pt: 'Tempo limite de conexão',
				ro: 'Conexiune timeout'
			},
			
	BADTAXON: { en: 'Unrecognizable taxonomy',
				bg: 'Неразпознаваема таксономия',
				de: 'Nicht erkennbare Taxonomie',
				es: 'Taxonomía irreconocible',
				fr: 'Taxonomie non reconnaissable',
				it: 'Tassonomia irriconoscibile',
				nl: 'Onherkenbaar taxonomie',
				pt: 'Taxonomia irreconhecível',
				ro: 'Taxonomie de nerecunoscut'
			},
			
	FILEDROP: {	en: 'Drop here a taxonomy',
				bg: 'Пуснете тук таксономия',
				de: 'Schau mal hier, eine Taxonomie',
				es: 'Colocar aquí una taxonomía',
				fr: 'Déposer ici une taxonomie',
				it: 'Rilascia qui una tassonomia',
				nl: 'Drop hier een taxonomie',
				pt: 'Largar aqui uma taxonomia',
				ro: 'Arunca aici o taxonomie'
			},

}