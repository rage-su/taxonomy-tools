/*	========================================================

	TAXONOMY.JS
	
	This file implements the frontend functionality of
	the RAGE Taxonomy Tools.
	
	License: to be defined
	
	Project RAGE (No 644187)
	Realising and Applied Gaming Eco-system
	Research and Innovation Action

	2015 P. Boytchev
	
	--------------------------------------------------------
		
	options{}
	
	new TaxonomyTool()
		static
		prototype
			localize()
			nextLanguage()
			attachTaxonomy(taxon)
		inherited
			new TaxonomyViewer(htmlId,taxonId)
			new TaxonomySelector(htmlId,taxonId)
	========================================================
*/



// Global options extracted from document's URL
options = {};
(window.onpopstate = function () {
	var match,
		pl     = /\+/g,  // Regex for replacing addition symbol with a space
		search = /([^&=]+)=?([^&]*)/g,
		decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
		query  = window.location.search.substring(1);

	while (match = search.exec(query))
	   options[decode(match[1])] = decode(match[2]);
})();



/*	========================================================
	TaxonomyTool

	Class. Implements common UI functions of all RAGE
	Taxonomy Tools.

	TaxonomyTool(htmlId,taxonId)

	Constructor. Creates the core of a taxonomy	tool object. 
		htmlId	String. The value of the Id attribute of an
				HTML element containing the taxonomy tool.
		taxonId	String. A taxonomy Id to fetch automatically
				from the server.

	Returns constructed taxonomy tool.
	========================================================
*/

function TaxonomyTool(htmlId,taxonId)
{
	this.elem = document.getElementById(htmlId);
	this.elem.classList.add('taxonomyBox');
	
	// recreate the internal structure of a tool
	var that = this;

	// create the tool header, containing
	this.header = document.createElement('div');
	this.header.className = 'header';
		// RAGE logo
		var logo = document.createElement('img');
		logo.className = 'logo';
		logo.src = '../images/rage-logo.png';
		this.header.appendChild(logo);
		
		// the title
		this.title = document.createElement('h2');
		this.title.innerHTML = '';
		this.header.appendChild(this.title);

		//	button OK
		this.buttonOK = document.createElement('img');
		this.buttonOK.className = 'button';
		this.buttonOK.src = '../images/button-ok.png';
		this.header.appendChild(this.buttonOK);
	
		//	button Cancel
		this.buttonCancel = document.createElement('img');
		this.buttonCancel.className = 'button cancel';
		this.buttonCancel.src = '../images/button-cancel.png';
		this.header.appendChild(this.buttonCancel);
	this.elem.appendChild(this.header);

	// create the container of the nodes and the canvas
	this.container = document.createElement('div');
	this.container.className = 'container';
		this.canvas = document.createElement('canvas');
		this.canvas.innerHTML = 'The canvas element is not supported!';
		this.container.appendChild(this.canvas);
		this.ctx = this.canvas.getContext('2d');
		
		this.info = document.createElement('h2');
		this.info.className = 'message';
		this.info.innerHTML = '&nbsp';
		this.info.style.display = 'none';
		this.container.appendChild(this.info);

		//
		this.container.addEventListener('dragenter', function(event){
			that.fileDragEnter(event);
		}, true);
		this.container.addEventListener('dragleave', function(event){
			that.fileDragLeave(event);
		}, true);
		this.container.addEventListener('dragover', function(event){
			event.stopPropagation();
			event.preventDefault();
		}, true);
		this.container.addEventListener('drop', function(event)
		{
			that.fileDragCount = 0;
			if (event.dataTransfer.files.length)
			{	// drop a file
				event.stopPropagation();
				event.preventDefault();

				var reader = new FileReader();
				reader.addEventListener('loadend', function(e, file)
				{
					var newTaxon = new Taxonomy();
					newTaxon.tool = that;
					newTaxon.loadFromString(this.result);
					that.attachTaxonomy(newTaxon);
				},false);
				reader.readAsText(event.dataTransfer.files[0]); 
				return false;
			}
			else
			{	// drop a node
				that.taxon.dragDone(event);
			}
		}, false);
	this.elem.appendChild(this.container);

	// create the footer with buttons
	this.footer = document.createElement('div');
	this.footer.className = 'footer';
		// use acem-rypy flag (if available)
		if ('acem-rypy' in options)
		{
			// button for resetting IDs
			var resetID = document.createElement('span');
			resetID.className = 'button';
			resetID.innerHTML = 'ID';
			resetID.addEventListener('click',function(){that.taxon.resetIds();});
			this.footer.appendChild(resetID);

			// button for exporting in JSON-LD format
			var expJSON = document.createElement('span');
			expJSON.className = 'button';
			expJSON.innerHTML = 'JSON';
			expJSON.addEventListener('click',function(){Taxonomy.download(that.taxon.getName()+'.json',that.taxon.saveToString());});
			this.footer.appendChild(expJSON);

			// button for exporting in TXT format
			var expTXT = document.createElement('span');
			expTXT.className = 'button';
			expTXT.innerHTML = 'TXT';
			expTXT.addEventListener('click',function(){Taxonomy.download(that.taxon.getName()+'.txt',that.taxon.saveToText());});
			this.footer.appendChild(expTXT);

			var bar = document.createElement('span');
			bar.innerHTML = ' | ';
			this.footer.appendChild(bar);
		}
		
		// button for changing the layout
		this.buttonLayout = document.createElement('span');
		this.buttonLayout.className = 'button';
		this.buttonLayout.innerHTML = 'Layout';
		this.buttonLayout.style.display = 'none';
		this.buttonLayout.addEventListener('click',function(){that.taxon.nextLayout();});
		this.footer.appendChild(this.buttonLayout);

		// button for changing the language
		this.buttonLang = document.createElement('span');
		this.buttonLang.className = 'button';
		this.buttonLang.innerHTML = 'Language';
		this.buttonLang.addEventListener('click',function(){that.nextLanguage();});
		this.footer.appendChild(this.buttonLang);
	this.elem.appendChild(this.footer);

	// use language from URL options (if available)
	this.lang = options['lang'] || 'en';

	// loading a taxonomy
	this.taxon = null;
	if (taxonId)
	{
		this.attachTaxonomy( new Taxonomy() );
		this.taxon.loadFromServer(taxonId,function(){that.taxonomyLoaded();});
	}
}



/*	--------------------------------------------------------
	TaxonomyTool.mouseClick(event,that)

	Method. Localizes the UI of the tool depending on the
	current language.
	--------------------------------------------------------
*/
TaxonomyTool.prototype.mouseClick = function(event,that)
{
	if (this.taxon)
	{
		this.taxon.currentNode = undefined;
		this.taxon.clearSelected();
		this.taxon.showButtonDelete();
	}
}



/*	--------------------------------------------------------
	TaxonomyTool.localize()

	Method. Localizes the UI of the tool depending on the
	current language.
	--------------------------------------------------------
*/
TaxonomyTool.prototype.localize = function()
{
	// localize the title
	if (TaxonomyLang.TITLE)
		this.title.innerHTML = TaxonomyLang.TITLE[this.lang] || TaxonomyLang.TITLE['en'];
		
	// localize the buttons
	this.buttonLayout.innerHTML = TaxonomyLang.LAYOUT[this.lang] || TaxonomyLang.LAYOUT['en'];
	this.buttonLang.innerHTML = (TaxonomyLang.LANGUAGE[this.lang] || TaxonomyLang.LANGUAGE['en'])+'('+(this.lang||'en')+')';
}



/*	--------------------------------------------------------
	TaxonomyTool.nextLanguage()

	Method. Switches to the next available language.
	--------------------------------------------------------
*/

TaxonomyTool.prototype.nextLanguage = function ()
{
	// get a list of languages
	var langs;
	if (this.taxon)
		langs = Object.keys(this.taxon.langs);
	else
		langs = Object.keys(TaxonomyLang.TITLE);
	
	var n = (langs.indexOf(this.lang)+1)%langs.length;
	this.lang = langs[n];
	this.localize();
	
	if (this.taxon)
		this.taxon.localize();
}



/*	--------------------------------------------------------
	TaxonomyTool.attachTaxonomy(taxon)

	Method. Attaches taxonomy object to a taxonomy tool.
	Creates UI elements for all taxonomy nodes.
		taxon	taxonomy instance to attach
	--------------------------------------------------------
*/
TaxonomyTool.prototype.attachTaxonomy = function (taxon)
{
	// valid taxonomy?
	if (!taxon instanceof Taxonomy)
		return;

	if (this.taxon) this.taxon.destroyDomNodes(this.container);
	this.taxon = taxon;
	this.taxon.tool = this;

	this.taxonomyLoaded();
}



/*	--------------------------------------------------------
	TaxonomyTool.taxonomyLoaded()

	Method. Executed when a taxonomy is being automatically
	loaded.
	--------------------------------------------------------
*/
TaxonomyTool.prototype.taxonomyLoaded = function ()
{
	if (this.taxon.state!=Taxonomy.STATE.READY)
		return;

	// if the language is not enforced, pick english (if
	// available), or any other (if it is not available)
	if (this instanceof TaxonomyEditor)
		this.taxon.langs = {bg:true,de:true,en:true,es:true,fr:true,it:true,nl:true,pt:true,ro:true};
	// take languages from underlying taxonomy
	var langIds = Object.keys(this.taxon.langs);
	if (!options['lang'] && !this.taxon.langs[this.lang])
	{
		this.lang = this.taxon.langs['en'] ? 'en':langIds[0];
		this.localize();
	}
	
	this.buttonLayout.style.display = 'inline-block';
	this.buttonLang.style.display = (langIds.length>1)?'inline-block':'none';
	
	this.taxon.createDomNodes(this.container);
	
	// in Taxonomy Editor mark all nodes
	if (this instanceof TaxonomyEditor)
	{
		this.taxon.selectStyle(Taxonomy.BOX.HANDLE,Taxonomy.EXCLUDE.NONE);
	}

	this.taxon.fixVisibility();
	this.taxon.draw();
}



/*	========================================================
	TaxonomyViewer

	Class. Descendent of TaxonomyTool. The class implements
	the RAGE Taxonomy Viewer.

	TaxonomyViewer(htmlId,taxonId)

	Constructor. Creates a taxonomy	viewer object. 
		htmlId		String. The value of the Id attribute of an
					HTML element containing the taxonomy viewer.
		taxonId		String. A taxonomy ID.
		
	Returns constructed taxonomy viewer.
	========================================================
*/
function TaxonomyViewer(id,taxonId)
{
	TaxonomyTool.call(this,id,taxonId);
	
	this.buttonOK.style.visibility = 'hidden';
	this.buttonCancel.style.visibility = 'hidden';
	
	TaxonomyLang.TITLE = TaxonomyLang.VIEWER;
	this.localize();
}
TaxonomyViewer.prototype = Object.create(TaxonomyTool.prototype);



/*	========================================================
	TaxonomySelector

	Class. Descendent of TaxonomyTool. The class implements
	the RAGE Taxonomy Editor.

	TaxonomySelector(id)

	Constructor. Creates a taxonomy	selector object. 
		htmlId		String. The value of the Id attribute of an
					HTML element containing the taxonomy selector.
		taxonId		String. A taxonomy ID.

	Returns constructed taxonomy selector.
	========================================================
*/
function TaxonomySelector(id,taxonId)
{
	TaxonomyTool.call(this,id,taxonId);
	
	TaxonomyLang.TITLE = TaxonomyLang.SELECTOR;
	this.localize();
}
TaxonomySelector.prototype = Object.create(TaxonomyTool.prototype);



/*	========================================================
	TaxonomyEditor

	Class. Descendent of TaxonomyTool. The class implements
	the RAGE Taxonomy Editor.

	TaxonomyEditor(id)

	Constructor. Creates a taxonomy	editor object. 
		htmlId		String. The value of the Id attribute of an
					HTML element containing the taxonomy editor.
		taxonId		String. A taxonomy ID.

	Returns constructed taxonomy editor.
	========================================================
*/
function TaxonomyEditor(id,taxonId)
{
	TaxonomyTool.call(this,id,taxonId);
	
	var that = this;
	this.buttonDelete = document.createElement('img');
	this.buttonDelete.className = 'button';
	this.buttonDelete.src = '../images/button-delete.png';
	this.buttonDelete.style.display = 'none';
	this.buttonDelete.addEventListener('click',function(){that.taxon.deleteNode();});
	this.container.appendChild(this.buttonDelete);

	// only in Editor - clicking inside emoty area of the
	// container clears the currently selected items
	this.container.addEventListener('click',function(event){that.mouseClick(event,that);});

	TaxonomyLang.TITLE = TaxonomyLang.EDITOR;
	this.localize();
}
TaxonomyEditor.prototype = Object.create(TaxonomyTool.prototype);




/*	--------------------------------------------------------
	TaxonomyTool.fileDragEnter()
	TaxonomyTool.fileDragLeave()

	Callback methods. Manage file drag-and-drop into a tool.
	--------------------------------------------------------
*/
TaxonomyTool.prototype.fileDragEnter = function(event)
{
	if (event.dataTransfer.files.length==0) return; // it is node dragging
	
//	this.container.style.backgroundColor = 'Blue';
//	this.container.style.border = 'Tomato solid 1px';
	if (this.fileDragCount)
		this.fileDragCount++;
	else
		this.fileDragCount = 1;
//	console.log(this.fileDragCount);

	if (this.fileDragCount==1)
	{
		this.taxon.showMessage('message','filedrop','1s',TaxonomyLang.FILEDROP);
	}
	
	//event.stopPropagation();
	event.preventDefault();
}
TaxonomyTool.prototype.fileDragLeave = function(event)
{
	if (event.dataTransfer.files.length==0) return; // it is node dragging

	this.fileDragCount--;
//	console.log(this.fileDragCount);

	if (this.fileDragCount<1)
	{
		this.taxon.fixVisibility();
		this.taxon.hideMessage();
		this.taxon.draw();
	}
	
	//event.stopPropagation();
	event.preventDefault();
}


